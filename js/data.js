var labels = []
var chart_data = []
var earthquakes = []
var places = []
var lugares = []
var cantidad_lugares = []
var amounts = []
var regiones_grafico4 = []
var chart4_data = []
var costos = []
var bombas = []
var response


$(function() {
    $.getJSON("https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_month.geojson", function( data ) {
        response = data.features

        $.each(response, function (index, value) {
            earthquakes.push({
                mag: value.properties.mag,
                place: value.properties.place.split("of").pop(),
                cdi: value.properties.cdi
            })
        })

        // set charts data
        $.each(earthquakes, function (index, value) {
            labels.push(value.place)
        })
        $.each(earthquakes, function (index, value) {
            chart_data.push(value.mag)
        })
        loadChart1()
    })

    $.getJSON("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&minlatitude=-49&maxlatitude=-18&minlongitude=-77&maxlongitude=-69", function (data) {
        response = data.features
        var place

        $.each(response, function (index, value) {
            place = value.properties.place.split("of").pop().split(",")[0]
            pos = $.inArray(place, places)
            if (pos > -1) {
                amounts[pos]++
            } else {
                places.push(place)
                amounts.push(1)
            }
        })
        loadChart2()
    })

    $.getJSON("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit=100&offset=1", function (data) {
        response = data.features
        var place
        var pos

        $.each(response, function (index, value) {
            place = value.properties.place.split("of").pop().split(",")[1]
            pos = $.inArray(place, lugares)
            if (pos > -1) {
                cantidad_lugares[pos]++
            } else {
                lugares.push(place)
                cantidad_lugares.push(1)
            }
        })
        loadChartPie()
    })

    $.getJSON("js/sismos.json", function (data) {
        regiones_grafico4.push(['País', 'Magnitud'])
        $.each(data, function (index, value) {
          regiones_grafico4.push([value.country, value.magnitud])
        })
        loadChart3()
    });

    $.getJSON("js/muertos.json", function (data) {
      chart4_data.push(['Pais', 'Muertos', 'Magnitud'])

      $.each(data, function (index, value){
        chart4_data.push([value.country, value.muertos, value.mag])
      })

      loadChart4()
    })

    $.getJSON("js/costos.json", function (data) {
      costos.push(['Sismo', 'Costo'])
      $.each(data, function (index, value){
        costos.push([
          "mag: " + value.mag + ", " + value.country + " - " + value.year,
          value.cost,
        ])
      })
      loadChart5()
    })

    $.getJSON("js/bombas.json" , function(data) {
        bombas.push(["Fechas", "Magnitud Sismos", "Bombas"])
        $.each(data, function(index,value){
            bombas.push([
                value.Fecha, value.MagSismos , value.Bombas
            ])
        })
    })
    loadChart6()
})
